__author__ = 'localadmin'
import sys
import datetime
sys.path.append('/libsvm-3.20/python')
from svmutil import *
f=open("./Test_EcoFlex_Silgum_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_EcoFlex_Silgum.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_EcoFlex_Silgum_T_novel.txt')
m = svm_train(y, x,'-c 128')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
	i=i+1
	if i<=1000:
		print len(p_val)
		list="1"+","+str(info[0])+"\n"
		f.write(list)
	else:
		list="-1"+","+str(info[0])+"\n"
                f.write(list)
#2##############################
f=open("./Test_Gelatine_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Gelatine_EcoFlex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Gelatine_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
##3################################
f=open("./Test_Gelatine_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Gelatine_Latex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Gelatine_Latex_T_novel.txt')
m = svm_train(y, x,'-c 128')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
###4###################################
f=open("./Test_Gelatine_WoodGlue_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Gelatine_WoodGlue.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Gelatine_WoodGlue_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
####5######################################
f=open("./Test_Latex_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Latex_EcoFlex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Latex_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)

#6###################
f=open("./Test_Silgum_Gelatine_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Silgum_Gelatine.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Silgum_Gelatine_T_novel.txt')
m = svm_train(y, x,'-c 8')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
#7######################
f=open("./Test_Silgum_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_Silgum_Latex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_Silgum_Latex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
#######8######
f=open("./Test_WoodGlue_EcoFlex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_WoodGlue_EcoFlex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_WoodGlue_EcoFlex_T_novel.txt')
m = svm_train(y, x,'-c 2')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
##9####################
f=open("./Test_WoodGlue_Latex_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_WoodGlue_Latex.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_WoodGlue_Latex_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)
#10###########################
f=open("./Test_WoodGlue_Silgum_T_novel_result.txt",'a')
y, x = svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Train_WoodGlue_Silgum.txt')
y1, x1= svm_read_problem('/home/pli7/amy/github/reid/siaseme/biometrics/Test_WoodGlue_Silgum_T_novel.txt')
m = svm_train(y, x,'-c 32')
p_label, p_acc, p_val = svm_predict(y1, x1, m)
i=0
for info in p_val:
        i=i+1
        if i<=1000:
                print len(p_val)
                list="1"+","+str(info[0])+"\n"
                f.write(list)
        else:
                list="-1"+","+str(info[0])+"\n"
                f.write(list)





