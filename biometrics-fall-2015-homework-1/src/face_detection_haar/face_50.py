__author__ = 'foury'
import numpy as np
import os
import cv2
from matplotlib import pyplot as plt

fold_number=1

face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')



################################detection part
img2 = cv2.imread("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/bitbucket/biometrics-fall-2015-homework-1/data/faces/IMG_1021.JPG")
img=cv2.resize(img2,None,fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)##

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
faces = face_cascade.detectMultiScale(gray,1.3,0)############################
rectList,weights=cv2.groupRectangles(np.array(faces).tolist(),3,1)#######################cluster the detect window return the density as weight
i=0
if len(rectList)>0:

    result=list()
    rectList_new = rectList.tolist()



    for (x,y,w,h) in rectList:


        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),3)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        #score=str(round(float(weights[i])/len(faces),2))
        score=str(int(weights[i]))
        cv2.putText(img,score,(x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0))
        i=i+1


    cv2.imshow('img',img)
    cv2.imwrite("1"+".jpg", img)

    print weights
    cv2.waitKey(10000)
    #cv2.destroyAllWindows()



