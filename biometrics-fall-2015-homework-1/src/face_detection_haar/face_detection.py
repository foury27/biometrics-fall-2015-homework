__author__ = 'foury'
import numpy as np
import cv2
from matplotlib import pyplot as plt

fold_number=1
while fold_number<10:
    face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')
    #eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
    f=open("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/FDDB-folds/FDDB-fold-"+"0"+str(fold_number)+".txt",'r')
    #f_rname=("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/rename.txt",'w')
    f_w=open("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/test"+"0"+str(fold_number)+".txt",'w')
    img_base='/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/FDDB-folds/'
    img_format='.ppm'
    j=0
    for line in f.readlines():
        j=j+1
        img_path=img_base+line[:-1]+img_format
        img_name=line

        print img_path

    ################################detection part
        img= cv2.imread(img_path)
        #img=cv2.resize(img_ori,None,fx=0.4, fy=0.4, interpolation = cv2.INTER_CUBIC)##
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray,1.3,0)############################
        rectList,weights=cv2.groupRectangles(np.array(faces).tolist(),3,1)#######################cluster the detect window return the density as weight
        if len(rectList)>0:
            result=list()
            rectList_new = rectList.tolist()
            f_w.write(img_name+str(len(rectList))+'\n')

            i=0
            for (x,y,w,h) in rectList:


                cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),3)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = img[y:y+h, x:x+w]
                #score=str(round(float(weights[i])/len(faces),2))
                score=str(int(weights[i]))
                cv2.putText(img,score,(x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0))

                i=i+1
                f_w.write(str(x)+' '+str(y)+' '+str(w)+' '+str(h)+' '+score+'\n')

            #cv2.imshow('img',img)

            cv2.imwrite(str(j)+".jpg", img)
            print weights
            #cv2.waitKey(1)
            #cv2.destroyAllWindows()
        else:


            f_w.write(img_name+"0"+'\n')
    fold_number=fold_number+1
###############################---for ten

face_cascade = cv2.CascadeClassifier('/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
f=open("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/FDDB-folds/FDDB-fold-"+str(fold_number)+".txt",'r')
#f_rname=("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/rename.txt",'w')
f_w=open("/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/test"+str(fold_number)+".txt",'w')
img_base='/Users/foury/Documents/works_documents/courses_fall_2015/biometrics/FDDB-folds/'
img_format='.ppm'
for line in f.readlines():
    img_path=img_base+line[:-1]+img_format
    img_name=line

    print img_path

################################detection part
    img= cv2.imread(img_path)
    #img=cv2.resize(img_ori,None,fx=0.4, fy=0.4, interpolation = cv2.INTER_CUBIC)##
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray,1.3,0)
    rectList,weights=cv2.groupRectangles(np.array(faces).tolist(),2,0.5)
    if len(rectList)>0:
        result=list()
        rectList_new = rectList.tolist()
        f_w.write(img_name+str(len(rectList))+'\n')

        i=0
        for (x,y,w,h) in rectList:


            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            #score=str(round(float(weights[i])/len(faces),2))
            score=str(int(weights[i]))
            cv2.putText(img,score,(x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0))
            i=i+1
            f_w.write(str(x)+' '+str(y)+' '+str(w)+' '+str(h)+' '+score+'\n')

        #cv2.imshow('img',img)
        print weights
        #cv2.waitKey(1)
        #cv2.destroyAllWindows()
    else:


        f_w.write(img_name+"0"+'\n')

