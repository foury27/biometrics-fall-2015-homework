__author__ = 'foury'
import csv

def csv_writer(data, path):
    """
    Write data to a CSV file path
    """
    with open(path, "wb") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for line in data:
            writer.writerow(line)

csvfile=[]

lines = csv.reader(open("./pairsDevTrain.txt"),delimiter="\t")
type=".jpg"
for line in lines:
    if len(line)==3:
        name=line[0]
        img1_number=line[1]
        img2_number=line[2]

        if int(img1_number)<10:
            img1_number="00"+img1_number
	elif int(img1_number)>=10 and int(img1_number)<100:
	    img1_number="0"+img1_number
        if int(img2_number)<10:
            img2_number="00"+img2_number
        elif int(img2_number)>=10 and int(img2_number)<100:
            img2_number="0"+img2_number



        img1_path="./lfw2/"+name+"/"+name+"_0"+img1_number+type
        img2_path="./lfw2/"+name+"/"+name+"_0"+img2_number+type
        row=img1_path+" "+img2_path+" "+str(1)#1 is same
        csvfile.append(row.split(" "))
    



    else:
        name_1=line[0]
        img1_number=line[1]
        name_2=line[2]
        img2_number=line[3]
	
        if int(img1_number)<10:
            img1_number="00"+img1_number
	elif int(img1_number)>=10 and int(img1_number)<100:
            img1_number="0"+img1_number
        if int(img2_number)<10:
            img2_number="00"+img2_number
	elif int(img2_number)>=10 and int(img2_number)<100:
            img2_number="0"+img2_number



        img1_path="./lfw2/"+name_1+"/"+name_1+"_0"+img1_number+type
        img2_path="./lfw2/"+name_2+"/"+name_2+"_0"+img2_number+type
        row=img1_path+" "+img2_path+" "+str(0)#0 is diff
        csvfile.append(row.split(" "))
print csvfile
csv_writer(csvfile,"./csv_train.txt")
